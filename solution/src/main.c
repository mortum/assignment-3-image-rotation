#include <stdio.h>

#include "./hfiles/argument_validator.h"
#include "./hfiles/bmp_handler.h"
#include "./hfiles/file_handler.h"
#include "./hfiles/rotator.h"

int main(int argc, char **argv) {
    struct image img;

    if (argc != 4) {
        perror("Wrong argument count!");
        return -1;
    };

    if (!validate_angle(argv[3])) {
        perror("Wrong angle value!");
        return -1;
    }

    FILE* source = open_file_read(argv[1]);
    enum read_status read_st = from_bmp(source, &img);

    if (read_st != READ_OK) {
        switch (read_st) {
            case READ_INVALID_SIGNATURE:
                perror("BMP invalid signature\n");
                break;
            case READ_INVALID_HEADER:
                perror("Cannot BMP file header\n");
                break;
            case READ_INVALID_BITS:
                perror("Unable to read BMP pixels data\n");
                break;
            case MEMORY_ALLOCATION_ERROR:
                perror("Unable to allocate memory for image\n");
                break;
            default:
                break;
        }
        return -1;
    }
    struct image rotated_img = rotate(&img, argv[3]);
    FILE* destination = open_file_write(argv[2]);

    enum write_status write_st = to_bmp(destination, &rotated_img);
    deinit_image(&rotated_img);
    if (write_st != WRITE_OK){
    switch (write_st) {
        case WRITE_ERROR_BMP_HEADER:
            perror("Unable to write bmp header\n");
            break;
        case WRITE_ERROR_BMP_BODY:
            perror("Unable to write bmp body\n");
            break;
        default:
            break;
        }
    }
    return 0;
}
