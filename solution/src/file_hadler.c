#include "./hfiles/file_handler.h"

FILE* open_file_read(const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (!file) {
        perror("Smth went wrong while reading file");
    }
    return file;
}

FILE* open_file_write(const char* filename) {
    FILE* file = fopen(filename, "wb");
    if (!file) {
        perror("Smth went wrong while writing to file");
    }
    return file;
}
