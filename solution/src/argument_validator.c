#include "./hfiles/argument_validator.h"
#include <stdlib.h>

const int ANGLES[] = {0, 90, -90, 180, -180, 270, -270};

int validate_angle(const char *angle) {
    long angle_int = strtol(angle, 0, 10);
    const int LEN_ANGLES = sizeof(ANGLES) / sizeof(ANGLES[0]);

    for (int i = 0; i < LEN_ANGLES; i++) {
        if (angle_int == ANGLES[i]) {
            return 1;
        }
    }
    return 0;
}
