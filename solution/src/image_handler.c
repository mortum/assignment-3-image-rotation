#include "./hfiles/image_handler.h"
#include <stdlib.h>

struct image init_image(const uint64_t width, const uint64_t height) {
    uint32_t size = sizeof(struct pixel) * width * height;
    struct image init_image = (struct image) {width, height, (struct pixel*)malloc(size)};
    if (!init_image.data) {
        return (struct image) {0};
    }
    return init_image;
}

void deinit_image( struct image *img ) {
    if (img->data) {
        free(img->data);
        img->data = 0;
    }
}
