#include "./hfiles/rotator.h"
#include <stdlib.h>

struct image rotate_by_90(struct image* source) {
    struct image new_img = init_image(source->height, source->width);
    if (source->data && new_img.data) {
        uint64_t y, x;
        for (y = 0; y < source->height; y++) {
            for (x = 0; x < source->width; x++) {
                new_img.data[x * source->height + (source->height - y - 1)] = source->data[y * source->width + x];
            }
        }
    }
    deinit_image(source);
    return new_img;
}

struct image rotate(struct image* source, const char* angle) {

    char *p;
    long converted = strtol(angle, &p, 10);
    int32_t angle_int = ((int32_t) converted);
    if (angle_int < 0) angle_int = 360 + angle_int;
    uint16_t times = angle_int / 90;

    struct image rotated = *source;
    for (uint16_t i = 0; i < times; ++i) {
        rotated = rotate_by_90(&rotated);
    }
    return rotated;
}
