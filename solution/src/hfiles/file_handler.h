#ifndef IMAGE_TRANSFORMER_FILE_HANDLER_H
#define IMAGE_TRANSFORMER_FILE_HANDLER_H

#include <stdio.h>

FILE* open_file_write(const char* filename);

FILE* open_file_read(const char* filename);
#endif //IMAGE_TRANSFORMER_FILE_HANDLER_H
