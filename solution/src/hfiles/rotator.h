#ifndef IMAGE_TRANSFORMER_ROTATOR_H
#define IMAGE_TRANSFORMER_ROTATOR_H

#include "image_handler.h"
#include <inttypes.h>

struct image rotate(struct image *source, const char* angle);

struct image rotate_by_90(struct image* source);
#endif
