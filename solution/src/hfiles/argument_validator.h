#ifndef IMAGE_TRANSFORMER_ARGUMENT_VALIDATOR_H
#define IMAGE_TRANSFORMER_ARGUMENT_VALIDATOR_H
#include <inttypes.h>

int validate_angle(const char *angle);
#endif
