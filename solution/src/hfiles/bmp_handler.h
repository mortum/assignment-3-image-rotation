#ifndef IMAGE_TRANSFORMER_BMP_HANDLER_H
#define IMAGE_TRANSFORMER_BMP_HANDLER_H

#include "image_handler.h"
#include  <stdint.h>
#include <stdio.h>

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    MEMORY_ALLOCATION_ERROR
};

enum read_status from_bmp(FILE* in, struct image* img);

enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR_BMP_HEADER,
    WRITE_ERROR_BMP_BODY
};

enum write_status to_bmp(FILE* out, struct image const* img);
#endif //IMAGE_TRANSFORMER_BMP_HANDLER_H
