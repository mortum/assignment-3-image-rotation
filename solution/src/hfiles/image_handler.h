#ifndef IMAGE_TRANSFORMER_IMAGE_HANDLER_H
#define IMAGE_TRANSFORMER_IMAGE_HANDLER_H

#include <inttypes.h>

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image init_image(uint64_t width, uint64_t height);

void deinit_image( struct image *img );

#endif
