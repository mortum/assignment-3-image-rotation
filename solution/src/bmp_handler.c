#include "./hfiles/bmp_handler.h"
#include <stdio.h>

#define BMP_SIG 0x4d42
#define X_PELS_P_METER 2835
#define RESERVED 0
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define Y_PELS_P_METER 2835
#define CLR_USED 0
#define CLR_IMPORTANT 0

struct __attribute__((packed)) bmp_header{
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

uint8_t get_padding_size(const uint64_t width) {
    return (4 - (width * sizeof(struct pixel)) % 4) % 4;
}

enum read_status read_data_pos_h(FILE* in, struct image* img, uint8_t padding_size) {
    for (int i = 0; i < img->height; i++) {
        if (!fread(&img->data[(img->height - i - 1) * img->width],
                   sizeof(struct pixel), img->width, in)) {
            deinit_image(img);
            return READ_INVALID_BITS;
        }
        fseek(in, padding_size, SEEK_CUR);
    }
    fclose(in);

    return READ_OK;
}

enum read_status read_data_neg_h(FILE* in, struct image* img, uint8_t padding_size) {
    for (int i = 0; i < img->height; i++) {
        if (!fread(&img->data[i * img->width], sizeof(struct pixel), img->width, in)) {
            deinit_image(img);
            return READ_INVALID_BITS;
        }
        fseek(in, padding_size, SEEK_CUR);
    }
    fclose(in);
    return READ_OK;
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;

    if (!img) return MEMORY_ALLOCATION_ERROR;

    if (!fread(&header, sizeof(struct bmp_header), 1, in)) return READ_INVALID_HEADER;

    if (header.bfType != BMP_SIG) return READ_INVALID_SIGNATURE;

    if (header.biHeight > 0) {
        *img = init_image(header.biWidth, header.biHeight);
    } else {
        *img = init_image(header.biWidth, -1 * header.biHeight);
    }

    uint8_t padding_size = get_padding_size(img -> width);
    if (header.biHeight > 0) {
        return read_data_pos_h(in, img, padding_size);
    }
    else {
        return read_data_neg_h(in, img, padding_size);
    }
}

enum write_status to_bmp(FILE* out, struct image const* img) {

    uint8_t row_padding = get_padding_size(img->width);
    struct bmp_header header;

    uint32_t img_size = (uint32_t) (img->height * (img->width * sizeof(struct pixel) + row_padding));
    uint32_t file_size = (uint32_t) sizeof(struct bmp_header);
    uint32_t off_bits = sizeof(struct bmp_header);
    uint32_t size = sizeof(struct bmp_header) - 14;
    uint32_t width = (uint32_t) img->width;
    uint32_t height = (uint32_t)img->height;

    header.bfType = BMP_SIG;
    header.biSizeImage = img_size;
    header.bfileSize = file_size;
    header.bfReserved = RESERVED;
    header.bOffBits = off_bits;
    header.biSize = size;
    header.biWidth = width;
    header.biHeight = height;
    header.biPlanes = PLANES;
    header.biBitCount = BIT_COUNT;
    header.biCompression = COMPRESSION;
    header.biXPelsPerMeter = X_PELS_P_METER;
    header.biYPelsPerMeter = Y_PELS_P_METER;
    header.biClrUsed = CLR_USED;
    header.biClrImportant = CLR_IMPORTANT;

    if (!fwrite(&header, sizeof(struct bmp_header), 1, out)) {
        return WRITE_ERROR_BMP_HEADER;
    }

    for (uint64_t i = img->height; i > 0; i--) {
        if (!fwrite(&img->data[(i - 1) * img->width], sizeof(struct pixel), img->width, out)) {
            return WRITE_ERROR_BMP_BODY;
        }

        for (uint64_t j = 0; j < row_padding; j++) {
            fputc(0, out);
        }
    }
    fclose(out);
    return WRITE_OK;
}

